# How to run this project locally

1. Install Dependencies using "npm install"
2. run the project using "npm run dev"

# How to run this project with docker

1. Install Docker on your local
2. Run this command in the root of project directory to build the image "docker build -t newsfeed-react ."
3. After image has been built , you can run the container using "docker run -p 8080:80 newsfeed-react"
4. you could access this on http://localhost:8080