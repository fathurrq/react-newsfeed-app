# Use an official Node runtime as a parent image
FROM node:14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json (or yarn.lock) into the container
COPY package*.json ./
# If you're using yarn, uncomment the next line and comment out the `npm install` line
# COPY yarn.lock ./

# Install any dependencies
RUN npm install
# For yarn, use the following command instead
# RUN yarn install

# Bundle app source inside the docker container
COPY . .

# Build your app
RUN npm run build

# Use the official nginx image as a base image
FROM nginx:stable-alpine

# Copy the build output to replace the default nginx contents.
COPY --from=0 /usr/src/app/dist /usr/share/nginx/html

# Expose port 80 to the outside once the container has launched
EXPOSE 80
