import { default as React, useContext, useEffect, useReducer, useState } from "react";
import { useNavigate } from "react-router-dom";
import { filterAndSliceArticles } from "../../../utils/filterAndSliceArticles/filterAndSliceArticles";
import { timeElapsedSince } from "../../../utils/timeElapsed/timeElapsed";
import CategoryHeader from "../../atoms/CategoryHeader/Header";
import Loader from "../../atoms/Loader/Loader";
import Card from "../../molecules/Card/Card";
import CategoryComponent from "../../molecules/Category/Category";
import HeaderNavigationMenu from "../../organisms/Navigation/HeaderNavigationMenu";
import { NewsAppContext } from "../../organisms/context/NewsAppContext";
import "./Home.styles.css";
import { SearchComponent } from "../../molecules/Search/Search";
import DatePicker from "../../atoms/DatePicker/DatePicker";
import { categoryTokens } from "../../../constants/categoryToken";
const HomeComponent: React.FC = () => {
  const navigate = useNavigate();
  const [selectedDate, setSelectedDate] = useState('')
  const {
    businessState,
    setBusinessState,
    entertainmentState,
    setEntertainmentState,
    healthState,
    setHealthState,
    scienceState,
    setScienceState,
    sportsState,
    setSportsState,
    technologyState,
    setTechnologyState,
  }: any = useContext(NewsAppContext);

  const initialState = {
    business: [],
    entertainment: [],
    health: [],
    science: [],
    sports: [],
    technology: [],
    loading: true,
    error: null,
  };

  const reducer = (state: any, action: any) => {
    switch (action.type) {
      case "FETCH_SUCCESS":
        return {
          ...state,
          [action.category]: action.articles,
          loading: false,
          error: null,
        };
      case "FETCH_ERROR":
        return {
          ...state,
          loading: false,
          error: action.error,
        };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  const API_KEY = import.meta.env.VITE_API_KEY;
  
  const handleDateChange = (date: string) => {
    setSelectedDate(date)
  }
  const fetchArticles = async (category: any) => {
    try {
      const response = await fetch(
        `https://newsapi.org/v2/top-headlines?country=us&category=${category}&apiKey=${API_KEY}&from=${selectedDate}`
      );
      const data = await response.json();
      switch (category) {
        case "business":
          setBusinessState(data.articles);
          break;
        case "entertainment":
          setEntertainmentState(data.articles);
          break;
        case "health":
          setHealthState(data.articles);
          break;
        case "science":
          setScienceState(data.articles);
          break;
        case "sports":
          setSportsState(data.articles);
          break;
        case "technology":
          setTechnologyState(data.articles);
          break;
        default:
          console.error(`Invalid category: ${category}`);
      }
      dispatch({ type: "FETCH_SUCCESS", category, articles: data.articles });
    } catch (error) {
      dispatch({ type: "FETCH_ERROR", error });
    }
  };

  useEffect(() => {
    categoryTokens.map((data) => {
      fetchArticles(data.categoryName.toLowerCase());
    })
  }, [selectedDate]);

  const { loading, error } = state;

  if (error) {
    return <div>Error: {error.message}</div>;
  }
  const categoryDetailPage = (val: any, data: any) => {
    const category = { title: val, data: data };
    navigate(`/detail/${val}`, { state: { category } });
  };
  return (
    <>
      {/* nav */}
      <HeaderNavigationMenu title={"Newsfeed App"} />
      <div className="search-button-wrapper">
        <SearchComponent />
      </div>
      <div className="search-button-wrapper">
        <DatePicker onChange={handleDateChange}/>
      </div>
      <CategoryComponent />
      <div className="flex-container">
        {/* left */}
        <div className="flex-item-left">
          <CategoryHeader
            title={"Technology"}
            onClick={() => categoryDetailPage("technology", technologyState)}
          />

          {loading ? (
            <Loader />
          ) : (
            filterAndSliceArticles(technologyState, 3).map(
              (article: any, index: number) => (
                <Card
                  key={index}
                  source={article.source.name}
                  url={article.url}
                  imageUrl={article.urlToImage}
                  title={article.title}
                  lastUpdated={timeElapsedSince(article.publishedAt)}
                />
              )
            )
          )}
        </div>
        {/* center */}

        <div className="flex-item-center">
          <CategoryHeader
            title={"Health"}
            onClick={() => categoryDetailPage("health", healthState)}
          />

          {loading ? (
            <Loader />
          ) : (
            filterAndSliceArticles(healthState, 3).map(
              (article: any, index: number) => (
                <Card
                  key={index}
                  source={article.source.name}
                  url={article.url}
                  imageUrl={article.urlToImage}
                  title={article.title}
                  lastUpdated={timeElapsedSince(article.publishedAt)}
                />
              )
            )
          )}
        </div>
        {/* right */}
        <div className="flex-item-right">
          <CategoryHeader
            title={"Science"}
            onClick={() => categoryDetailPage("science", scienceState)}
          />
          {/* <HorizontalLine color={'#EEEEEE'} height={2} /> */}

          {loading ? (
            <Loader />
          ) : (
            filterAndSliceArticles(scienceState, 3).map(
              (article: any, index: number) => (
                <Card
                  key={index}
                  source={article.source.name}
                  url={article.url}
                  imageUrl={article.urlToImage}
                  title={article.title}
                  lastUpdated={timeElapsedSince(article.publishedAt)}
                />
              )
            )
          )}
        </div>
      </div>
      <div className="flex-container">
        {/* left */}
        <div className="flex-item-left">
          <CategoryHeader
            title={"Sports"}
            onClick={() => categoryDetailPage("sports", sportsState)}
          />

          {loading ? (
            <Loader />
          ) : (
            filterAndSliceArticles(sportsState, 3).map(
              (article: any, index: number) => (
                <Card
                  key={index}
                  source={article.source.name}
                  url={article.url}
                  imageUrl={article.urlToImage}
                  title={article.title}
                  lastUpdated={timeElapsedSince(article.publishedAt)}
                />
              )
            )
          )}
        </div>
        {/* center */}

        <div className="flex-item-center">
          <CategoryHeader
            title={"Entertainment"}
            onClick={() =>
              categoryDetailPage("entertainment", entertainmentState)
            }
          />

          {loading ? (
            <Loader />
          ) : (
            filterAndSliceArticles(entertainmentState, 3).map(
              (article: any, index: number) => (
                <Card
                  key={index}
                  source={article.source.name}
                  url={article.url}
                  imageUrl={article.urlToImage}
                  title={article.title}
                  lastUpdated={timeElapsedSince(article.publishedAt)}
                />
              )
            )
          )}
        </div>
        {/* right */}
        <div className="flex-item-right">
          <CategoryHeader
            title={"Business"}
            onClick={() => categoryDetailPage("business", businessState)}
          />

          {loading ? (
            <Loader />
          ) : (
            filterAndSliceArticles(businessState, 3).map(
              (article: any, index: number) => (
                <Card
                  key={index}
                  source={article.source.name}
                  url={article.url}
                  imageUrl={article.urlToImage}
                  title={article.title}
                  lastUpdated={timeElapsedSince(article.publishedAt)}
                />
              )
            )
          )}
        </div>
      </div>
    </>
  );
};

export default HomeComponent;
