import React from "react";
import { useNavigate } from "react-router-dom";

import "./HeaderNavigationMenu.styles.css";

interface HeaderProps {
  title: string;
}

const HeaderNavigationMenu: React.FC<HeaderProps> = ({ title }) => {
  const navigate = useNavigate();
  const backToHome = () => {
    navigate("/");
  }
  return (
    <>
      <div className="header-navigation-menu-container">
        <div className="header-wrapper">
          <h1 className="title" onClick={backToHome}>{title}</h1>
        </div>
      </div>
    </>
  );
};

export default HeaderNavigationMenu;
