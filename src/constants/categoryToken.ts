export const categoryTokens = [
  { categoryName: 'Business' },
  { categoryName: 'Entertainment' },
  { categoryName: 'Health' },
  { categoryName: 'Science' },
  { categoryName: 'Sports' },
  { categoryName: 'Technology' },
];
