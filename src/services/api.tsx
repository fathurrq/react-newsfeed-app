import axios from 'axios';
export const BASE_URL = import.meta.env.BASE_URL;

export const api = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const getHeadlineByCountry = async () => {
  axios
    .get(
      'https://newsapi.org/v2/top-headlines?country=us&apiKey=f463419c4e4c4ebd96549c95688e979b'
    )
    .then((response: any) => {
      return response.data;
    })
    .catch((error: any) => {
      console.log(error);
    });
};

export const fetchSearchDataApi = async (query: any) => {
  const API_KEY = import.meta.env.VITE_API_KEY;
  try {
    const data = await axios.get(
      ` https://newsapi.org/v2/everything?q=${query}&sortBy=popularity&apiKey=${API_KEY}`
    );
    return data;
  } catch (error) {
    console.log(error);
  }
};
