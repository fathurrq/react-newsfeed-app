import {useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import {categoryTokens} from '../../../constants/categoryToken';
import ButtonComponent from '../../atoms/Button/Button';
import {NewsAppContext} from '../../organisms/context/NewsAppContext';
import './Category.styles.css';
const CategoryComponent = () => {
  const navigate = useNavigate();
  const {
    businessState,
    entertainmentState,
    healthState,
    scienceState,
    sportsState,
    technologyState,
  }: any = useContext(NewsAppContext);
  const categoryDetailPage = (categoryName: any, data: any) => {
    const category = {title: categoryName, data: data};
    navigate(`/detail/${categoryName}`, {state: {category}});
  };
  const handleCategory = (categoryName: any) => {
    switch (categoryName) {
      case 'Business':
        categoryDetailPage(categoryName, businessState);
        break;
      case 'Entertainment':
        categoryDetailPage(categoryName, entertainmentState);
        break;
      case 'Health':
        categoryDetailPage(categoryName, healthState);
        break;
      case 'Science':
        categoryDetailPage(categoryName, scienceState);
        break;
      case 'Sports':
        categoryDetailPage(categoryName, sportsState);
        break;
      case 'Technology':
        categoryDetailPage(categoryName, technologyState);
        break;
      default:
        console.error(`Invalid category: ${categoryName}`);
    }
  };

  return (
    <section className='category-wrapper'>
      {categoryTokens.map(({categoryName}) => (
        <ButtonComponent
          key={categoryName}
          onClick={() => handleCategory(categoryName)}>
          {categoryName}
        </ButtonComponent>
      ))}
    </section>
  );
};

export default CategoryComponent;
