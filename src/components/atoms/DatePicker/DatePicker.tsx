import React, { useState } from "react";
import "./DatePicker.styles.css";

interface DatePickerProps {
  onChange: (date: string) => void;
}
const DatePicker: React.FC<DatePickerProps> = ({ onChange }) => {
  const [selectedDate, setSelectedDate] = useState<string>("");

  const handleDateChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    const formattedDate: string = event.target.value;
    onChange(formattedDate);
    setSelectedDate(formattedDate);
  };

  return (
    <div className="input-wrapper">
      Select Date :
      <input
        type="date"
        placeholder="Select Date"
        value={selectedDate}
        onChange={handleDateChange}
      />
    </div>
  );
};

export default DatePicker;
